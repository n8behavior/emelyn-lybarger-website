---
title: "About"
date: 2018-07-12T18:19:33+06:00
heading : "Get into the habit of asking yourself, ‘Does this support the life I’m trying to create?"
description : "Hi! I’m Emelyn Lybarger, a Life & Leadership coach supporting women in their personal and professional lives to achieve their goals. Do you want stronger leadership capacity? Greater resilience in the face of life’s challenges and transitions? Deeper connection with yourself and in your relationships? I’ll assist you step-by-step to move forward."
expertise_title: "More about me"
expertise_sectors: >
    My coaching is holistic, using emotional intelligence and mindfulness. I work with your thinking, emotional, and physical self, and the ways they inform each other, as I support you to achieve your goals. This methodology helps create the sustainable change so many people want, but often have difficulty obtaining. What’s that about, anyway?! I’ll help you explore that, as well!

    Get in touch here.

    I have an M.A. in Intercultural Communications from the University of Maryland, Baltimore County. I’m a certified Somatic Coach from the Strozzi Institute and a certified Internal Conflict Coach from Pamela J. Green Solutions. Since 2009, I have studied under Suzanne Roberts of Unifying Solutions. I began providing life and leadership coaching to clients in 2012, and began working in team development in 2016.
---
